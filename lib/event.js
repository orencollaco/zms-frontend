
export const EventTypeMap = Object.freeze({
    0:  "Unspecified",
    1:  "Other",
    2:  "Created",
    3:  "Updated",
    4:  "Deleted",
    5:  "Revoked",
    6:  "Enabled",
    7:  "Disabled",
    8:  "Approved",
    9:  "Denied",
    10: "Scheduled",
    11: "Running",
    12: "Completed",
    13: "Failed",
    14: "Succeeded",
    15: "Progress",
    16: "Suspended",
    17: "Resumed",
    18: "Started",
    19: "Stopped",
    20: "Violation",
    21: "Interference",
    22: "Action",
    23: "Pending",
})

export const EventSourceTypeMap = Object.freeze({
    0:  "Unspecified",
    1:  "Identity",
    2:  "Zmc",
    3:  "Dst",
    4:  "Alarm",
    5:  "Ra",
})

export const EventCodeMap = Object.freeze({
    0:    "Unspecified",
    1001: "User",
    1002: "Element",
    1003: "Role",
    1004: "RoleBinding",
    1005: "Token",
    1006: "Service",
    2001: "Zone",
    2002: "Antenna",
    2003: "Radio",
    2004: "RadioPort",
    2005: "Monitor",
    2006: "Grant",
    2007: "Spectrum",
    3001: "Observation",
    3002: "Annotation",
    3003: "Propsim",
    3004: "PropsimJob",
    4001: "ConstraintChange",
    4002: "GrantChange",
    4003: "RAObservation"
})

export const EventCodeIconMap = Object.freeze({
    0:    "mdi-help",
    1001: "mdi-account",
    1002: "mdi-account-group",
    1003: "mdi-shield-key",
    1004: "mdi-key-link",
    1005: "mdi-key",
    1006: "mdi-cog",
    2001: "mdi-earth",
    2002: "mdi-antenna",
    2003: "mdi-radio-tower",
    2004: "mdi-radio-tower",
    2005: "mdi-signal",
    2006: "mdi-clock-outline",
    2007: "mdi-broadcast",
    3001: "mdi-database",
    3002: "mdi-database",
    3003: "mdi-map-outline",
    3004: "mdi-map-outline",
    4001: "mdi-clock-minus-outline",
    4002: "mdi-clock-alert-outline"
})

export function NewUUID() {
    if (typeof crypto !== 'undefined' && typeof crypto.randomUUID !== 'undefined')
        return crypto.randomUUID()
    else
        return URL.createObjectURL(new Blob()).substr(-36)
}

export class ZmsEvent {

    constructor(evt) {
        this.header = evt.header
        this.object = evt.object
    }

    getTypeName() {
      return EventTypeMap[this.header?.type] || this.header?.type
    }

    getCodeName() {
        return EventCodeMap[this.header?.code] || this.header?.code
    }

    getCodeIcon() {
        return EventCodeIconMap[this.header?.code] || "mdi-help"
    }

    getSourceTypeName() {
        return EventSourceTypeMap[this.header?.source_type] || this.header?.source_type
    }

    getObjectId() {
        return this.header?.object_id;
    }

    getObjectName() {
        if (!this.header?.object_id)
            return ""
        return this.object?.name || this.object?.description || this.header?.object_id
    }

    getObjectIcon() {
        return EventCodeIconMap[this.header?.code] || "mdi-help"
    }

    getTitle() {
        return this.getCodeName() + " " + this.getObjectName() + " " + this.getTypeName()
    }

    getObjectLink() {
        if (!this.header?.object_id)
            return ""

        switch (this.header?.code) {
        case 1001:
            return `/users/${this.header.object_id}`
        case 1002:
            return `/elements/${this.header.object_id}`
        case 1004:
            if (this.header?.user_id)
                return `/users/${this.header.user_id}`
            return
        case 1005:
            if (this.header?.user_id)
                return `/users/${this.header.user_id}`
            return ""
        case 2002:
            return `/antennas/${this.header.object_id}`
        case 2003:
            return `/radios/${this.header.object_id}`
        case 2004:
            if (this.object?.radio_id)
                return `/radios/${this.object.radio_id}/ports/${this.header.object_id}`
        case 2005:
            return `/monitors/${this.header.object_id}`
        case 2006:
            return `/grants/${this.header.object_id}`
        case 2007:
            return `/spectrum/${this.header.object_id}`
        case 3001:
            return `/observations/${this.header.object_id}`
        case 3003:
            return `/propsims/${this.header.object_id}`
        default:
            return ""
        }
    }

    getAlertType() {
        if (this.header?.type === 5 || this.header?.type === 13
            || this.header?.type === 20)
            return "error"
        else if (this.header?.type === 7 || this.header?.type === 9
                 || this.header?.type === 16 || this.header?.type === 21)
            return "warning"
        else if (this.header?.type === 2 || this.header?.type === 6
                 || this.header?.type === 8 || this.header?.type === 10
                 || this.header?.type === 11 || this.header?.type === 12
                 || this.header?.type === 14 || this.header?.type === 17
                 || this.header?.type === 18 || this.header?.type === 19
                 || this.header?.type === 22 || this.header?.type === 23) {
            // Ensure Observation events are only present in last tier.
            if (this.header?.code !== 3001)
                return "success"
            else
                return "info"
        }
        else
            return "info"
    }
}

export class ZmsSubscription {

    constructor(service, msgHandler, errHandler = null, options = {}) {
        this.service = service
        this.filters = null
        this._options = options
        this._msgHandler = msgHandler
        this._errHandler = errHandler
        this.status = "disconnected"
        this.connected = false
        this.error = null
        this._socket = null
        this._subscriptionRequest = null
        this._subscriptionResponse = null
        this._backendUrl = null
        this._id = null

        if (!(service === "identity" || service === "zmc"
              || service === "dst" || service === "alarm"))
            throw new Error(`invalid service '${this.service}'`)

        if (service == "identity")
            // NB: Once https://github.com/unjs/nitro/issues/678 is
            // resolved, we can proxy these sockets through nitro, like
            // useNuxtApp().$config.public.origin + '/ws/identity/v1'
            // with the enabling corresponded commented routeRules entry
            // in nuxt.config.ts .
            this._backendUrl = useNuxtApp().$config.public.identityUrl
        else if (service == "zmc")
            this._backendUrl = useNuxtApp().$config.public.zmcUrl
        else if (service == "dst")
            this._backendUrl = useNuxtApp().$config.public.dstUrl
        else if (service == "alarm")
            this._backendUrl = useNuxtApp().$config.public.alarmUrl
        else
            throw new Error(`no backend URL configured for service '${this.service}'`)

        this._backendUrl = this._backendUrl.replace("http:", "ws:").replace("https:", "wss:")
    }

    async open(filters = null) {
        this.filters = filters

        // Remove an existing subscription; we utilize the server's
        // typical remove-subscription-on-websocket-close semantics.
        if (this.status !== "disconnected") {
            try {
                await this.close()
            } catch (e) {
                console.log(`Subscription.open[${this.service}]: error in pre-connect close:`, e)
            }
        }

        this.connected = false
        this._socket = null
        this.error = null
        this.status = "subscribing"

        this._id = NewUUID()
        let data = { id: this._id, filters: this.filters }
        let thisSub = this
        let lopts = {
            method: 'POST',
            body: JSON.stringify(data),
            async onRequestError({ request, options, error }) {
                console.log("Subscription.open:requestError", error)
                thisSub.error = error
                // Make sure the socket is cleaned up before we tell handlers.
                await thisSub.close()
                if (this._errHandler)
                    this._errHandler(thisSub.error)
            },
            async onResponse({ request, options, response }) {
                this._subscriptionResponse = response
                if (response.ok) {
                    console.log("Subscription.open:response", response)
                    thisSub.status = "subscribed"
                    return thisSub.stream()
                }
            },
            async onResponseError({ request, options, response }) {
                this._subscriptionResponse = response
                console.log("Subscription.open:responseError", response)
                thisSub.error = {
                    code: response.status,
                    message: response.statusText,
                    messageDetail: response._data?.message,
                    summary: `${response.statusCode}: ${response.statusText} (${response._data?.message})`,
                }
                // Make sure the socket is cleaned up before we tell handlers.
                await thisSub.close()
                if (this._errHandler)
                    this._errHandler(thisSub.error)
            },
        }
        this._subscriptionResponse = useNuxtApp().$fetch(
            `/zms/${this.service}/subscriptions`,
            Object.assign({}, this._options, lopts))
    }

    async stream() {
        // NB: a time-honored strategy of websocket auth is stashing
        // credentials into the Sec-Websocket-Protocols header, since the
        // WebSocket API does not allow custom headers (e.g. X-Api-Token).
        // Our backends honor this.
        var token = null
        if (useNuxtApp().$clientStore.is_admin)
            token = useNuxtApp().$userStore.admin_token.token
        else
            token = useNuxtApp().$userStore.token.token

        this._socket =
            new WebSocket(this._backendUrl + '/subscriptions/' + this._id + '/events', [token,])

        this._socket.onopen = (evt) => {
            console.log(`Subscription.stream[${this.service}]:onopen`, evt)
            this.status = "connected"
            this.connected = true
        }

        let thisSub = this

        this._socket.onmessage = (evt) => {
            console.log(`Subscription.stream[${thisSub.service}]:onmessage`, evt)
            if (!evt.data || evt.data === 'null')
                return
            if (typeof evt.data !== 'string')
                return
            try {
                var j = JSON.parse(evt.data)
                console.log(`Subscription.stream[${thisSub.service}]:onmessage: json`, j)
                if (j && typeof j == 'object')
                    thisSub._msgHandler(new ZmsEvent(j))
            } catch (e) {
                console.log(`Subscription.stream[${thisSub.service}]:onmessage: decode error`, e)
            }
        }

        // Websocket API does not give good errors via the handler, so we
        // fake it.
        this._socket.onclose = this._socket.onerror = async (evt) => {
            if (thisSub.status === "unsubscribing" || thisSub.status === "disconnected")
                return
            console.log(`Subscription.stream[${thisSub.service}]:errh: closed unexpectedly`, evt)
            thisSub._socket = null
            thisSub.status = "disconnected"
            thisSub.error = {
                code: 999,
                message: "Websocket error",
                messageDetail: "",
                summary: "Websocket error",
            }
            // Make sure the socket is cleaned up before we tell handlers.
            await thisSub.close()
            if (this._errHandler)
                this._errHandler(thisSub.error)
        }
    }

    async close() {
        this.status = "unsubscribing"
        if (this._socket) {
            try {
                this._socket.close()
            } catch (e) {
                console.log(`Subscription.close[${this.service}]: error`, e)
            }
            this._socket = null
        }
        else if (this._id) {
            // If we never got a websocket, we can still delete the
            // subscription at the server before we try again.
            let subId = this._id
            useNuxtApp().$fetch(
                `/zms/${this.service}/subscriptions/${subId}`,
                { method: 'DELETE', body: JSON.stringify({}), })
                .catch(e => console.log("Subscription: error deleting subscription", subId, e))
                .then(data => console.log("Subscription: deleted subscription", subId))
        }
        this.connected = false
        this.status = "disconnected"
        this._subscriptionRequest = null
        this._subscriptionResponse = null
        this._id = null
        //this.error = null
    }
}
