import * as echarts from 'echarts'
import * as _ from 'underscore';
import Papa from 'papaparse';

export default function createFrequencyGraph(observation) {
    console.info("createFrequencyGraph", observation);
    clearFrequencyGraph();

    var papaResults =
        Papa.parse(atob(observation.data),
                   {
                       dynamicTyping : true,
                       skipEmptyLines: true,
                       header : true,
                   });
    var data = papaResults.data;

    if (data.length > 1000) {
        createBinGraph(createBins(data));
    }
    else {
        createSubGraph(data);
    }
}

function clearFrequencyGraph()
{
    console.info("clearFrequencyGraph");

    var mainDiv = document.querySelector(".frequency-graph-maingraph .echarts-container");
    var subDiv  = document.querySelector(".frequency-graph-subgraph .echarts-container");

    if (mainDiv) {
        echarts.dispose(mainDiv);
    }
    if (subDiv) {
        echarts.dispose(subDiv);
    }
}

function updateToolTip(tip, field, value)
{
    var td = tip.querySelector(field);
    td.innerHTML = value;
}

function createBinGraph(data)
{
    var graph = document.querySelector(".frequency-graph-maingraph");
    graph.style.display = null;
    
    // Create the echarts instance
    var myChart = echarts.init(graph.querySelector(".echarts-container"));

    // Need to expicitly resize.
    window.onresize = function() {
        myChart.resize();
    };    
   
    // We clone the tooltip from the callback below.
    var tip = document.querySelector(".frequency-graph-maingraph .popover-content");

    window.mychart = myChart;
    
    /*
     * Need a map from the integer frequency to the bin.
     */
    var binmap  = {};
    _.each(data, function (bin) {
        var freq = bin.frequency;
        binmap[freq] = bin;
    });

    var subGraphData = function (center_freq)
    {
        console.info(center_freq);
        var bin     = binmap[center_freq];
	var subdata = [];
	var min     = center_freq - 25;
        var max     = center_freq + 25;
        
	for (var i = min; i < max; i++) {
            if (_.has(binmap, i)) {
		// Hmm, the CSV file appears to not be well sorted within
		// a frequency bin. Must be a string sort someplace.
		var sorted = binmap[i].samples
		    .sort(function (a, b) { return a.frequency - b.frequency});
		subdata = subdata.concat(sorted);
            }
	}
        return subdata;
    };

    /*
     * Click on the tiny little circle for this event. The "bin"
     * is in the params structure. Yippie.
     */
    myChart.on('click', function(params) {
        console.info(params);
        createSubGraph(subGraphData(params.data.frequency));
    });
    /*
     * For events over the "blank" area we get an event and it is not
     * super helpful. But the chart exports a function to do it for us.
     */
    myChart.getZr().on('click', function(event) {
        console.info(event);
        var point = [event.offsetX, event.offsetY];
        var grid  = myChart.convertFromPixel('grid', point);
        var freq  = Math.floor(grid[0]);
        
        createSubGraph(subGraphData(freq));
    });
    
    // Draw the chart
    myChart.setOption({
        animation: false,
        dataset: {
            source: data,
        },
        grid: {
            top: 10,
            bottom: 80,
            left: 60,
            right: 10,
        },
        tooltip: {
            trigger: "axis",
            padding: 2,
            axisPointer: {
                type: "line",
                axis: "x",
                snap: true,
            },
            formatter: function (params) {
                //console.info(params);
                var data = params[0].data;
                var tipc = tip.cloneNode(true);
                updateToolTip(tipc, ".tooltip-frequency", data.frequency);
                updateToolTip(tipc, ".tooltip-avg", data.avg.toFixed(3));
                updateToolTip(tipc, ".tooltip-max", data.max.toFixed(3));
                updateToolTip(tipc, ".tooltip-min", data.min.toFixed(3));
                return tipc;
            },
            shadowColor: "rgba(0, 0, 0, 0)",
            shadowBlur: 0,  
            borderWidth: 1,
            borderColor: "grey",
        },
        xAxis: {
            name: "Frequency (MHz)",
            nameLocation: "middle",
            nameGap: 20,
            max: "dataMax",
            min: "dataMin",
        },
        yAxis: {
            name: "Power (dBX)",
            nameLocation: "middle",
            nameGap: 40,
            max: "dataMax",
            min: "dataMin",
            axisLabel: {
                formatter: function (value) {
                    return value.toFixed(1);
                },
            },
        },
        dataZoom: [
            {
                type: 'inside',
            },
            {
                type: 'slider',
                height: 40,
            },
        ],
        series: [
            {
                type: 'line',
                showSymbol: false,
                symbolSize: 1,
                encode: {
                    x: "frequency",
                    y: "avg",
                },
            },
        ],
    });

}

function createSubGraph(data)
{
    console.info("subgraph", data);

    // Must destroy first.
    echarts.dispose(document.querySelector(".frequency-graph-subgraph .echarts-container"));

    var graph = document.querySelector(".frequency-graph-subgraph");
    graph.style.display = null;
    
    // Create the echarts instance
    var myChart = echarts.init(graph.querySelector(".echarts-container"));

    // Need to expicitly resize.
    window.onresize = function() {
        myChart.resize();
    };    
   
    // We clone the tooltip from the callback below.
    var tip = document.querySelector(".frequency-graph-subgraph .popover-content");

    // These fields might not be present.
    for (var i = 0; i < data.length; i++) {
        var d = data[i];
        if (!_.has(d, "abovefloor")) {
            d.abovefloor = 0;
        }
        if (!_.has(d, "violation")) {
            d.violation = 0;
        }
    }

    // Draw the chart
    myChart.setOption({
        animation: false,
        dataset: {
            source: data,
        },
        grid: {
            top: 10,
            bottom: 80,
            left: 60,
            right: 10,
        },
        tooltip: {
            trigger: "axis",
            padding: 2,
            axisPointer: {
                type: "line",
                axis: "x",
                snap: true,
            },
            formatter: function (params) {
                //console.info(params);
                var data = params[0].data;
                var tipc = tip.cloneNode(true);
                updateToolTip(tipc, ".tooltip-frequency", data.frequency.toFixed(3));
                updateToolTip(tipc, ".tooltip-power", data.power.toFixed(3));
                updateToolTip(tipc, ".tooltip-center", data.center_freq.toFixed(3));
                updateToolTip(tipc, ".tooltip-floor", data.abovefloor.toFixed(3));
                updateToolTip(tipc, ".tooltip-violation", data.violation);
                if (_.has(data, "kurtosis")) {
                    updateToolTip(tipc, ".tooltip-kurtosis", data.kurtosis.toFixed(3));
                }
                return tipc;
            },
            shadowColor: "rgba(0, 0, 0, 0)",
            shadowBlur: 0,  
            borderWidth: 1,
            borderColor: "grey",
        },
        xAxis: {
            name: "Frequency (MHz)",
            nameLocation: "middle",
            nameGap: 20,
            min: (data.length == 1 ? data[0].frequency - 100000 : "dataMin"),
            max: (data.length == 1 ? data[0].frequency + 100000 : "dataMax"),
        },
        yAxis: {
            name: "Power (dBX)",
            nameLocation: "middle",
            nameGap: 40,
            min: "dataMin",
            max: "dataMax",
            axisLabel: {
                formatter: function (value) {
                    return value.toFixed(1);
                },
            },
        },
        dataZoom: [
            {
                type: 'inside',
            },
            {
                type: 'slider',
                height: 40,
            },
        ],
        series: [
            {
                type: 'line',
                showSymbol: (data.length == 1 ? true : false),
                symbolSize: 3,
                encode: {
                    x: "frequency",
                    y: "power",
                },
            },
        ],
    });
}

function createBins(data)
{
    var result = [];
    var bins   = [];
    var hasAboveFloor= (_.has(data[0], "abovefloor") ? true : false);
    console.info("CreateBins: ", data);

    _.each(data, function (d, index) {
	var freq  = +d.frequency;
	var power = +d.power;
	var x     = Math.floor(freq);

	if (!_.has(bins, x)) {
	    var bin = {
		"frequency" : x,
		"max"       : power,
		"min"       : power,
		"avg"       : power,
		"samples"   : [d],
	    };
	    if (hasAboveFloor) {
		bin["abovefloor"] = d.abovefloor;
		bin["violation"]  = d.violation;
		if (d.abovefloor) {
		    console.info(bin);
		}
	    }
	    bins[x] = bin;
	    result.push(bin);
	    return;
	}
	var bin = bins[x];
	if (power > bin.max) {
	    bin.max = power;
	}
	if (power < bin.min) {
	    bin.min = power;
	}
	if (hasAboveFloor) {
	    if (d.abovefloor > bin.abovefloor) {
		bin.abovefloor = d.abovefloor;
	    }
	    if (d.violation) {
		bin.violation = 1;
	    }
	    if (d.abovefloor) {
		console.info(bin);
	    }
	}
	bin.samples.push(d);
	var sum = 0;
	_.each(bin.samples, function (d) {
	    sum  += d.power;
	});
	bin.avg  = sum / _.size(bin.samples);
    });
    console.info("createBins", result);
    return result;
}

