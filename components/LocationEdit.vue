<template>
  <v-form
    v-if="item"
    v-model="isFormValid"
  >
    <v-card>
      <v-card-title>
        <span v-if="create">New Location</span>
        <span v-else>Edit Location <i>{{ item.name }}</i> ({{item.id}})</span>
      </v-card-title>
      <v-card-text
          style="font-size: 0.85rem"
          class="px-4 py-0"
      >
        <v-container class="pt-4">
          <v-row v-if="create">
            <v-col class="py-0 px-0">
              <v-select
                v-model="item.element_id"
                :items="allElements"
                item-title="name"
                item-value="id"
                label="Element"
                variant="outlined"
                density="compact"
                :rules="[rules.required]"
              ></v-select>
            </v-col>
          </v-row>
          <v-row v-else>
            <v-col class="py-0 px-0">
              <v-text-field
                  v-model="myelementname"
                  readonly
                  label="Element"
                  variant="outlined"
                  density="compact"
              ></v-text-field>
            </v-col>
          </v-row>
          <v-row>
            <v-col class="py-0 px-0">
              <v-text-field
                  v-model="item.name"
                  label="Name"
                  variant="outlined"
                  density="compact"
                  :rules="[rules.required, rules.nonzerolen, rules.morethanspace]"
              ></v-text-field>
            </v-col>
          </v-row>
          <v-row>
            <v-col class="py-0 px-0">
              <v-text-field
                  v-model.number="item.srid"
                  clearable
                  label="SRID"
                  variant="outlined"
                  density="compact"
                  :rules="[rules.required, rules.morethanspace]"
              ></v-text-field>
            </v-col>
          </v-row>
          <v-row>
            <v-col cols="2" class="pl-0">
              <v-text-field
                  v-model.number="item.x"
                  label="X (Longitude)"
                  variant="outlined"
                  density="compact"
                  :rules="[rules.required, rules.validLongitude]"
                ></v-text-field>
            </v-col>
            <v-col cols="2">
              <v-text-field
                  v-model.number="item.y"
                  label="Y (Latitude)"
                  variant="outlined"
                  density="compact"
                  :rules="[rules.required, rules.validLongitude]"
                ></v-text-field>
            </v-col>
            <v-col cols="2">
              <v-text-field
                  v-model.number="item.z"
                  label="Z (Elevation)"
                  variant="outlined"
                  density="compact"
              ></v-text-field>
            </v-col>
          </v-row>
        </v-container>
      </v-card-text>
      <v-card-actions class="px-6 py-0">
        <div v-if="create">
          <v-btn
            color="white"
            class="px-3 bg-success"
            background="success"
            elevation="2"
            :disabled="!isFormValid || disabled"
            >
            Create
            <Confirm activator="parent"
                     message="Are you sure you want to create this location?"
                     @confirmed="submit">
            </Confirm>
            
          </v-btn>
          <v-btn v-if="embedded"
            color="white"
            class="px-3 bg-warning"
            background="warning"
            elevation="2"
            :disabled="disabled"
            @click="notifyCanceled"
            >
            Cancel
          </v-btn>
        </div>
        <div v-else>
          <v-btn
            color="white"
            class="px-3 bg-success"
            background="success"
            elevation="2"
            :disabled="!isFormValid || disabled"
            >
            Save
            <Confirm activator="parent"
                     message="Are you sure you want to save these changes?"
                     @confirmed="save">
            </Confirm>
          </v-btn>
          <v-btn
            color="white"
            class="px-3 bg-warning ml-6"
            background="warning"
            elevation="2"
            :disabled="!isFormValid || disabled"
            >
            Delete
            <Confirm activator="parent"
                     message="Are you sure you want to delete this location?"
                     @confirmed="remove">
            </Confirm>
          </v-btn>
        </div>
      </v-card-actions>
    </v-card>
  </v-form>
  <ErrorDialog
    :model-value="errorIsActive"
    :error="error"
    @close="errorIsActive = false; this.error = null"
    @reload="errorIsActive = false; this.error = null"
    @back="errorIsActive = false; this.error = null"
    ></ErrorDialog>
</template>

<script>
  import { mapState } from 'pinia'
  import { clientStore } from '~/stores/client'
  import { adminStore } from '~/stores/admin'
  import { zoneStore } from '~/stores/zone'
  import { formRules } from '~/lib/formrules'
  import Confirm from '~/components/Confirm.vue'
  import ErrorDialog from '~/components/ErrorDialog.vue'
  import moment from 'moment'  
  import { getElementNameById } from '~/lib/helpers'

export default defineComponent({
    name: 'LocationEdit',
    components: {
        Confirm: () => import("@/components/Confirm")
    },
    props: {
        item: {
            type: Object,
            required: true
        },
        create: {
            type: Boolean,
            required: false,
            default: false
        },
        embedded: {
            type: Boolean,
            required: false,
            default: false
        }
    },
    data() {
        return {
            isFormValid: false,
            disabled: false,
            rules: formRules(),
            errorIsActive: false,
            error: null,
            unsaved: false,
        }
    },
    emits: ['modified', 'created', 'canceled'],
    async mounted() {
        console.log("LocationEdit:mounted")
    },
    computed: {
        ...mapState(clientStore, {
            is_admin: (state) => state.is_admin
        }),
        allElements() {
            if (this.is_admin)
                return this.$adminStore.all_elements.elements
            else
                return this.$userStore.elements
        },
        myelementname() {
            return getElementNameById(this.item.element_id);
        },
    },
    methods: {
        async save() {
            console.log("LocationEdit:Save")
            this.disabled = true
            let data = await this.$locationsEndpoint
                .update(this.item.id, this.item)
                .then((data) => {
                    this.notifyModified(data)
                })
                .catch((err) => {
                    this.error = err
                    this.errorIsActive = true
                    clearError()
                    return
                })
            this.disabled = false
        },
        async remove() {
            console.log("LocationEdit:Delete")
            this.disabled = true
            await this.$locationsEndpoint
                .delete(this.item.id, this.item).catch((err) => {
                    this.error = err
                    this.errorIsActive = true
                    clearError()
                })
            this.disabled = false
            navigateTo('/')
        },
        async submit() {
            this.disabled = true
            let data = await this.$locationsEndpoint
                .create(this.item)
                .then((data) => {
                    this.notifyCreated(data)
                })
                .catch((err) => {
                    this.error = err
                    this.errorIsActive = true
                    clearError()
                    return
                })
            this.disabled = false
        },
        notifyModified(x) {
            console.log("LocationEdit:notifyModified", x)
            let event = {
                id: x.id,
                item: x,
            }
            this.$emit('modified', event)
        },
        notifyCreated(x) {
            console.log("LocationEdit:notifyCreated", x)
            let event = {
                id: x.id,
                item: x,
            }
            this.$emit('created', event)
        },
        notifyCanceled() {
            console.log("LocationEdit:notifyCanceled")
            this.$emit('canceled')
        }
    },
    watch: {
        item: {
            deep: true,
            handler(val) {
                this.unsaved = true;
            }
        }
    },
});
</script>
