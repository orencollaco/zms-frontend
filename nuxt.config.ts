import vuetify from 'vite-plugin-vuetify'
import vuetifySass from '@paro-paro/vite-plugin-vuetify-sass'

export default defineNuxtConfig({
  dir: {
    public: 'static',
  },
  app: {
    head: {
      title: process.env.npm_package_name || 'OpenZMS',
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        {
          hid: 'description',
          name: 'description',
          content: process.env.npm_package_description || ''
        },
				{
          property: 'og:url',
          content: 'https://rdz.powderwireless.net/'
        },
        { property: 'og:type', content: 'website' },
        {
          property: 'og:title',
          content: 'POWDER-RDZ - POWDER Radio Dynamic Zone - OpenZMS'
        },
        {
          property: 'og:description',
          content: 'The POWDER-RDZ project investigates ways to share the electromagnetic (radio-frequency) spectrum between experimental or test systems and existing spectrum users, and between multiple experimental systems. This research team is deploying a prototype automatic spectrum sharing management system for the POWDER testbed in Salt Lake City, Utah (part of the NSF "Platforms for Advanced Wireless Research" program). Spectrum access challenges currently create significant constraints on experimentation and testing at wireless testbeds. Automatic spectrum sharing for safe access to additional frequencies beyond the frequencies reserved exclusively for testing will relax these constraints and thus increase the nation\'s capacity to conduct wireless research and development. Increasing this capacity will help accelerate growth and global leadership of the US communications industry, strengthen academic research into wireless systems, and benefit other spectrum-dependent sectors such as radar, public safety, and national defense. As a pathfinder for the National Radio Dynamic Zone concept, the project will help future federal/non-federal spectrum sharing arrangements assure that spectrum sharing does not negatively impact government users.  The POWDER-RDZ team is developing and deploying an end-to-end radio dynamic zone (RDZ) for advanced wireless communication. They will validate its functionality by performing spectrum sharing experiments and field studies on the resulting artifact. The project uses the existing POWDER mobile and wireless testbed as the physical infrastructure of the RDZ. POWDER\'s existing radios and other equipment supports the spectrum sharing experiments and provides part of the RF sensing functionality needed by the RDZ. The project is developing a modular zone management system (ZMS) to manage, control and monitor all aspects of the RDZ. The project plans to conduct experiments on spectrum sharing with users outside of POWDER. Experiments potentially include RDZ shared access to federal, non-federal, and commercial spectrum, such as coarse- and fine-grained spectrum sharing with a commercial mobile operator and spectrum sharing with a weather radar.'
        },
        {
          property: 'og:image',
          content: 'https://rdz.powderwireless.net/images/powderlogo-simple.svg'
        },
        { property: 'og:image:type', content: 'image/svg' },
        { property: 'og:image:alt', content: 'POWDER-RDZ Logo' }
			],
      link: [
        {
          rel: 'icon', type: 'image/x-png', href: '/favicon.png'
        }
      ],
		},
	},
  css: [
    'vuetify/lib/styles/main.sass',
    '@mdi/font/css/materialdesignicons.min.css',
    '~/assets/custom.css'
  ],
  runtimeConfig: {
    authSecret: process.env.AUTH_SECRET || '',
    authOrigin: process.env.AUTH_ORIGIN || '',
    githubClientId: process.env.GITHUB_CLIENT_ID || '',
    githubClientSecret: process.env.GITHUB_CLIENT_SECRET || '',
    googleClientId: process.env.GOOGLE_CLIENT_ID || '',
    googleClientSecret: process.env.GOOGLE_CLIENT_SECRET || '',
    cilogonClientId: process.env.CILOGON_CLIENT_ID || '',
    cilogonClientSecret: process.env.CILOGON_CLIENT_SECRET || '',
    public: {
      origin: process.env.ORIGIN || '',
      authOrigin: process.env.AUTH_ORIGIN || '',
      githubEnabled: process.env.GITHUB_ENABLED || false,
      googleEnabled: process.env.GOOGLE_ENABLED || false,
      cilogonEnabled: process.env.CILOGON_ENABLED || false,
      identityUrl: process.env.IDENTITY_URL || 'http://localhost:8000/v1',
      zmcUrl: process.env.ZMC_URL || 'http://localhost:8010/v1',
      dstUrl: process.env.DST_URL || 'http://localhost:8020/v1',
      alarmUrl: process.env.ALARM_URL || 'http://localhost:8030/v1',
      raUrl: process.env.RA_URL || 'http://localhost:8050/v1',
      geoserverUrl: process.env.GEOSERVER_URL || 'http://localhost:8080/geoserver',
      geoserverWorkspace: process.env.GEOSERVER_WORKSPACE || 'zms',
    },
    plugins: {
      nitro: {
        routeRules: {
          '/zms/identity/**': {
            proxy: {
              to: {
                runtimeConfigPublicField: "identityUrl",
                append: "/**",
              },
              changeOrigin: true
            }
          },
          '/zms/zmc/**': {
            proxy: {
              to: {
                runtimeConfigPublicField: "zmcUrl",
                append: "/**",
              },
              changeOrigin: true
            }
          },
          '/zms/dst/**': {
            proxy: {
              to: {
                runtimeConfigPublicField: "dstUrl",
                append: "/**",
              },
              changeOrigin: true
            }
          },
          '/zms/alarm/**': {
            proxy: {
              to: {
                runtimeConfigPublicField: "alarmUrl",
                append: "/**",
              },
              changeOrigin: true
            }
          },
          '/zms/ra/**': {
            proxy: {
              to: {
                runtimeConfigPublicField: "raUrl",
                append: "/**",
              },
              changeOrigin: true
            }
          },
        },
      },
    },
  },
  routeRules: {
  },
  build: {
    transpile: ['vuetify'],
  },
  hooks: {
    'vite:extendConfig': (config) => {
      config.plugins.push(
        vuetify({
          autoImport: true,
        }),
        vuetifySass({
          configFile: './assets/settings.scss'
        })
      )
    },
  },
  router: {
    mode: 'history'
  },
  modules: [
    '@sidebase/nuxt-auth',
    '@pinia/nuxt',
    '@pinia-plugin-persistedstate/nuxt'
  ],
  auth: {
    isEnabled: true,
    // NB: if you override baseURL at build-time, you cannot override later
    // at runtime via AUTH_ORIGIN (https://sidebase.io/nuxt-auth/configuration/nuxt-config).
    //baseURL: (process.env.ORIGIN || 'http://localhost:3000') + "/api/auth",
    // NB: you should never need to set `origin` explicitly here.
    //origin: process.env.ORIGIN || 'http://localhost:3000',
  },

  //
  // Debugging: reminder, these are build-time options, so require a rebuild
  // to modify.
  //
  //devtools: {
  //  enabled: true
  //},
  //logLevel: "verbose",
  //debug: true,
  // NB: should be set automatically
  //dev: true,
  //nitro: {
  //  logLevel: 4,
  //  dev: true,
  //},
})
