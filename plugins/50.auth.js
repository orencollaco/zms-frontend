//import type { GetServerSidePropsContext, InferGetServerSidePropsType } from "next";

export default defineNuxtPlugin(({ $userStore, $tokensEndpoint, $usersEndpoint }) => {
  const auth = useAuth()

  if (auth.status.value === 'authenticated' && $userStore && ($userStore.token === null || $userStore.token.id === 'undefined')) {
    //console.log('authenticated', useNuxtApp().ssrContext !== 'undefined' ? "server" : "client", auth.data.value)
    //console.log('userStore', $userStore)
    //console.log('userStore.token', $userStore.token)

    if (auth.data?.value?.provider !== 'credentials') {
      $tokensEndpoint.create({
        method: "idp",
        credential: {
          idp: auth.data?.value?.provider,
          access_token: auth.data?.value?.access_token,
        },
        admin_if_bound: true,
      }).then(response => {
        console.log("token", response)
        if (typeof response !== 'undefined' && response.user_id) {
          $userStore.user_id = response.user_id
          $userStore.token = response
        }
      })
    }
    else {
      if (auth.data.value.token !== 'undefined') {
        $userStore.user_id = auth.data.value.token.user_id
        $userStore.token = auth.data.value.token
      }
    }
  }

  return {
    provide: {
      auth: {
        ...auth,
        logout: auth.signOut,
        loginWith: auth.signIn,
        loginDirect: async (username, password) => {
          let token = await auth.actions.getCsrfToken()
          let headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            //...await auth.getRequestCookies(nuxt)
          }
          const body = new URLSearchParams({
            ...options,
            csrfToken,
            callbackUrl,
            json: true
          })
          let response = fetch('/api/auth/callback/credentials', {
            method: "post",
            params: authorizationParams,
            headers,
            body
          })
        }
      }
    }
  }
})
