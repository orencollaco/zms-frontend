import { createVuetify } from 'vuetify'
//import * as components from 'vuetify/components'
//import * as directives from 'vuetify/directives'

export default defineNuxtPlugin(nuxtApp => {
  const vuetify = createVuetify({
    ssr: true,
    components: {
    },
    theme: {
      defaultTheme: 'customLight',
      themes: {
        customLight: {
          dark: false,
          colors: {
            //primary: '#ff5722',
            primary: '#dd6031',
            background: '#ffffff',
            surface: '#ffffff',
            //'primary-darken-1': '#000000',
            //secondary: '#311e10',
            //secondary: '#6fa0bd',
            //secondary: '#eabe7c',
            //secondary: '#ece4b7',
            //secondary: '#d4cac7',
            secondary: '#607d8b',
            'secondary-lighten-5': '#f7f9fa',
            'secondary-lighten-4': '#eceff1',
            'secondary-lighten-3': '#cfd8dc',
            'secondary-lighten-2': '#b0bec5',
            'secondary-lighten-1': '#78909c',
            'secondary-darken-1': '#546e7a',
            'secondary-darken-2': '#455a64',
            'secondary-darken-3': '#37474f',
            'secondary-darken-4': '#263238',
            'secondary-darken-5': '#000000',
            error: '#b00020',
            info: '#5b99be',
            //success: '#d9dd92',
            //success: '#9ccc89',
            success: '#6f995f',
            warning: '#bd2828',
          },
        },
      },
      variations: {
        colors: ['primary'], //, 'secondary'],
        lighten: 5,
        darken: 5,
      },
    },
  })

  nuxtApp.vueApp.use(vuetify)

  return {
    provide: {
      vuetify: vuetify
    }
  }
})
