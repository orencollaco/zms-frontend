import createRepository from '~/api/repository'

export default defineNuxtPlugin(({ $fetch }) => {
  const zmsApiFactory = createRepository($fetch)

  return {
    provide: {
      tokensEndpoint: zmsApiFactory('zms/identity/tokens'),
      usersEndpoint: zmsApiFactory('zms/identity/users'),
      elementsEndpoint: zmsApiFactory('zms/identity/elements'),
      servicesEndpoint: zmsApiFactory('zms/identity/services'),
      rolesEndpoint: zmsApiFactory('zms/identity/roles'),
      roleBindingsEndpoint: zmsApiFactory('zms/identity/rolebindings'),
      spectrumEndpoint: zmsApiFactory('zms/zmc/spectrum'),
      locationsEndpoint: zmsApiFactory('zms/zmc/locations'),
      radiosEndpoint: zmsApiFactory('zms/zmc/radios'),
      antennasEndpoint: zmsApiFactory('zms/zmc/antennas'),
      grantsEndpoint: zmsApiFactory('zms/zmc/grants'),
      claimsEndpoint: zmsApiFactory('zms/zmc/claims'),
      monitorsEndpoint: zmsApiFactory('zms/zmc/monitors'),
      zonesEndpoint: zmsApiFactory('zms/zmc/zones'),
      observationsEndpoint: zmsApiFactory('zms/dst/observations'),
      raobservationsEndpoint: zmsApiFactory('zms/ra/raobservations'),
      propsimsEndpoint: zmsApiFactory('zms/dst/propsims'),
      servicesPropsimsEndpoint: zmsApiFactory('zms/dst/services/propsims'),
      grantAlarmsEndpoint: zmsApiFactory('zms/alarm/alarms/grants'),
    }
  }
})
