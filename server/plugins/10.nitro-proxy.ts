import { joinURL, withQuery, getQuery, withoutBase } from "ufo";
import {
    eventHandler,
    sendRedirect,
    setHeaders,
    proxyRequest
} from "h3";


export default defineNitroPlugin(async (nitroApp) => {
    const runtimeConfig = useRuntimeConfig()

    // NB: the h3-proxy way would be like this:
    //
    // import { createProxyEventHandler } from "h3-proxy";
    // if (runtimeConfig.plugins?.proxy?.options) {
    //   runtimeConfig.plugins?.proxy?.options.forEach(
    //     (item, index, arr) => nitroApp.hooks.hook('request', createProxyEventHandler(item)))
    // }
    //
    // but this should be a touch faster.

    let configRouteRules = runtimeConfig?.plugins?.nitro?.routeRules || {};
    console.log("[nitro-plugin-proxy] routeRules:", configRouteRules)

    for (const [path, inputRouteRules] of Object.entries(configRouteRules)) {
        // Support runtime-computed targets.

        // NB: non-extensible, read-only object, so ensure we can update.
        let routeRules = { ...inputRouteRules }
        if (routeRules.proxy) {
            routeRules.proxy = { ...routeRules.proxy }
            if (path.endsWith("/**")) {
                // Internal flag
                routeRules.proxy._proxyStripBase = path.slice(0, -3);
            }
        }
        // NB: although you see functions below, note that nuxt.config.ts
        // filters out function values in this part of the config, at least;
        // therefore, we have this icky hack that looks up a key in
        // runtimeConfig.public!
        if (routeRules.proxy?.to?.runtimeConfigPublicField) {
            console.log("[nitro-plugin-proxy]: dynamic target for path = " + path + ": runtimeConfigPublicField = " + routeRules.proxy.to.runtimeConfigPublicField)
            let newTo = runtimeConfig.public[routeRules.proxy.to.runtimeConfigPublicField]
            if (routeRules.proxy?.to?.append) {
                newTo += routeRules.proxy.to.append
            }
            routeRules.proxy.to = newTo
            console.log("[nitro-plugin-proxy]: dynamic target for path = " + path + ": set to " + routeRules.proxy.to)
        }

        // TBD: if nuxt.config.ts ever allows functions in the
        // runtimeConfig, this will work.
        if ((typeof routeRules.proxy?.to) === 'function') {
            console.log("[nitro-plugin-proxy]: dynamic target for path = " + path)
            let result = routeRules.proxy.to()
            routeRules.proxy.to = result
            console.log("[nitro-plugin-proxy]: dynamic target " + routeRules.proxy.to + " (" + path + ")")
        }
        if (typeof routeRules.redirect?.to === 'function') {
            routeRules.redirect.to = routeRules.redirect.to()
        }
        let handler = eventHandler((event) => {
            //console.log("[nitro-plugin-proxy]: request", event.req.url);

            /*
             * NB: the following code is lifted from
             * unjs/nitro/src/runtime/route-rules.ts:createRouteRulesHandler
             * -- no good way to do better, and we want the same behavior.
             * (see license/copyright in ./NITRO-LICENSE.txt .)
             *
             * NB: however, note that we are missing the behaviors in
             * unjs/nitro/src/options.ts:normalizeRouteRules, so none of the
             * shortcuts there work here -- you must be explicit in the
             * route behavior you want.
             */

            // Apply headers options
            if (routeRules.headers) {
                setHeaders(event, routeRules.headers);
            }
            // Apply redirect options
            if (routeRules.redirect) {
                return sendRedirect(
                    event,
                    routeRules.redirect.to,
                    routeRules.redirect.statusCode
                );
            }
            // Apply proxy options
            if (routeRules.proxy) {
                let target = routeRules.proxy.to;
                if (target.endsWith("/**")) {
                    let targetPath = event.path;
                    const strpBase = (routeRules.proxy as any)._proxyStripBase;
                    if (strpBase) {
                        targetPath = withoutBase(targetPath, strpBase);
                    }
                    target = joinURL(target.slice(0, -3), targetPath);
                } else if (event.path.includes("?")) {
                    const query = getQuery(event.path);
                    target = withQuery(target, query);
                }
                console.log("[plugin-nitro-proxy]: proxyRequest", event.path, "->", target)
                return proxyRequest(event, target, {
                    fetch: nitroApp.localFetch,
                    ...routeRules.proxy,
                });
            }
        })
        nitroApp.router.add(path, handler, 'all')
    }

})
