
import { defineStore } from 'pinia'

export const zoneStore = defineStore('zone', {
    state: () => ({
        user: {
            id: null,
            user: {},
            tokens: [],
            editable: false
        },
        users: {
            users: [],
            page: 1,
            pages: 1,
            total: 0
        },
        radio: {
            radio: null,
            editable: false,
        },
        radios: {
            radios: [],
            page: 1,
            pages: 1,
            total: 0
        },
        radioport: {
            radioport: null,
            editable: false,
        },
        element: {
            element: null,
            editable: false,
        },
        spectrum: {
            spectrum: null,
            editable: false,
        },
        spectrums: {
            spectrum: [],
            page: 1,
            pages: 1,
            total: 0
        },
        grant: {
            grant: null,
            editable: false,
        },
        grants: {
            grants: [],
            page: 1,
            pages: 1,
            total: 0
        },
        monitor: {
            monitor: null,
            editable: false,
        },
        monitors: {
            monitors: [],
            page: 1,
            pages: 1,
            total: 0
        },
        antenna: {
            antenna: null,
            editable: false,
        },
        antennas: {
            antennas: [],
            page: 1,
            pages: 1,
            total: 0
        },
        location: {
            location: null,
            editable: false,
        },
        locations: {
            locations: [],
            page: 1,
            pages: 1,
            total: 0
        },
        observations: {
            observations: [],
            page: 1,
            pages: 1,
            total: 0,
        },
        astronomy: {
            raobservations: [],
            page: 1,
            pages: 1,
            total: 0,
        },
        // If this starts out as {} we draw a bogus page until the
        // the observation comes back and triggers the update.
        observation: null,
        propsim: {
            propsim: null,
            editable: false,
        },
        propsims: {
            propsims: [],
            page: 1,
            pages: 1,
            total: 0
        },
        propsimJob: {
            propsimJob: null,
            editable: false,
        },
        servicesPropsims: {
            services: [],
            page: 1,
            pages: 1,
            total: 0
        },
        grantAlarm: null,
        zones: {
            zones: [],
            page: 1,
            pages: 1,
            total: 0
        },
        zone: {
            zone: null,
            data: null,
        },
    }),
    actions: {
        logout() {
            this.$reset()
        },
        resetUser() {
            this.user.id = null
            this.user.user = {}
            this.user.tokens = []
            this.user.editable = false
        },
        clearObservation() {
            this.observation = null;
        },
        async fetchUser(id, params = null) {
            if (id !== this.user.id)
                this.resetUser()

            let ctx = this
            let response = await useNuxtApp().$usersEndpoint.read(id, params, {
                async onResponse({ response }) {
                    ctx.user.editable = response.headers.get('X-Api-Can-Edit') || false
                }})
            if (typeof response !== 'undefined') {
                this.user.user = response
                console.log("zone.fetchUser response", response)
            }
            else {
                console.log("zone.fetchUser error", response)
            }
        },
        async fetchUserTokens(id, params = null) {
            if (id !== this.user.id)
                this.resetUser()

            let ctx = this
            let response = await useNuxtApp().$usersEndpoint.readSecondary(id, 'tokens', params, {
                async onResponse({ response }) {
                    ctx.user.editable = response.headers.get('X-Api-Can-Edit') || false
                }})
            if (typeof response !== 'undefined') {
                this.user.tokens = response.tokens
                console.log("zone.fetchUserTokens response", response)
            }
            else {
                console.log("zone.fetchUserTokens error", response)
            }
        },
        async fetchRadios(params = null) {
            let response = await useNuxtApp().$radiosEndpoint.list(params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchRadio response', response)
                this.radios = response
                console.log('zone.fetchRadio response', response)
            }
            else {
                console.log('zone.fetchRadio error', response)
            }
        },
        async fetchRadio(id, params = null) {
            let ctx = this
            let response = await useNuxtApp().$radiosEndpoint.read(id, params, {
                async onResponse({ response }) {
                    ctx.radio.editable = response.headers.get('X-Api-Can-Edit') || false
                }})
            if (typeof response !== 'undefined') {
                console.log('zone.fetchRadio response', response)
                this.radio.radio = response
                console.log('zone.fetchRadio response', response, this.radio.editable)
            }
            else {
                console.log('zone.fetchRadio error', response)
            }
        },
        async fetchRadioPort(radioid, portid, params = null) {
	    let path = radioid + "/ports/" + portid;
            let ctx = this
            let response = await useNuxtApp().$radiosEndpoint.read(path, params, {
                async onResponse({ response }) {
                    ctx.radioport.editable = response.headers.get('X-Api-Can-Edit') || false
                }})
            if (typeof response !== 'undefined') {
                console.log('zone.fetchRadio response', response)
                this.radioport.radioport = response
                console.log('zone.fetchRadio response', response, this.radio.editable)
            }
            else {
                console.log('zone.fetchRadio error', response)
            }
        },
        async fetchObservations(params = null) {
            let response = await useNuxtApp().$observationsEndpoint.list(params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchObservations response', response)
                this.observations = response
                console.log('zone.fetchObservations response', response)
            }
            else {
                console.log('zone.fetchObservations error', response)
            }
        },
        async fetchObservation(id, params = null) {
            let response = await useNuxtApp().$observationsEndpoint.read(id, params)
            if (typeof response !== 'undefined') {
                this.observation = response
                console.log('zone.fetchObservation response', response)
            }
            else {
                console.log('zone.fetchObservation error', response)
            }
        },
        async fetchRAObservations(params = null) {
            let response = await useNuxtApp().$raobservationsEndpoint.list(params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchRAObservations response', response)
                this.observations = response
                console.log('zone.fetchRAObservations response', response)
            }
            else {
                console.log('zone.fetchRAObservations error', response)
            }
        },
        async fetchRAObservation(id, params = null) {
            let response = await useNuxtApp().$raobservationsEndpoint.read(id, params)
            if (typeof response !== 'undefined') {
                this.raobservation = response
                console.log('zone.fetchRAObservation response', response)
            }
            else {
                console.log('zone.fetchRAObservation error', response)
            }
        },
        async fetchPropsims(params = null) {
            let response = await useNuxtApp().$propsimsEndpoint.list(params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchPropsims response', response)
                this.propsims = response
                console.log('zone.fetchPropsims response', response)
            }
            else {
                console.log('zone.fetchPropsims error', response)
            }
        },
        async fetchPropsim(id, params = null) {
            let ctx = this
            let response = await useNuxtApp().$propsimsEndpoint.read(id, params, {
                async onResponse({ response }) {
                    ctx.propsim.editable = response.headers.get('X-Api-Can-Edit') || false
                }})
            if (typeof response !== 'undefined') {
                this.propsim.propsim = response
                console.log('zone.fetchPropsim response', response, this.propsim.editable)
            }
            else {
                console.log('zone.fetchPropsim error', response)
            }
        },
        async fetchServicesPropsims(params = null) {
            let response = await useNuxtApp().$servicesPropsimsEndpoint.list(params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchServicesPropsims response', response)
                this.servicesPropsims = response
            }
            else {
                console.log('zone.fetchServicesPropsims error', response)
            }
        },
        getPropsimServiceById(id) {
            if (!this.servicesPropsims.services)
                return
            for (var i = 0; i < this.servicesPropsims.services.length; ++i) {
                if (this.servicesPropsims.services[i].id === id)
                    return this.servicesPropsims.services[i]
            }
            return null
        },
        async fetchPropsimJob(id, jobid, params = null) {
            let path = id + "/jobs/" + jobid
            let ctx = this
            let response = await useNuxtApp().$propsimsEndpoint.read(path, params, {
                async onResponse({ response }) {
                    ctx.propsim.editable = response.headers.get('X-Api-Can-Edit') || false
                }})
            if (typeof response !== 'undefined') {
                this.propsimJob.propsimJob = response
                console.log('zone.fetchPropsimJob response', response, this.propsimJob.editable)
            }
            else {
                console.log('zone.fetchPropsimJob error', response)
            }
        },
        async fetchGrantAlarm(id, params = null) {
            let ctx = this
            let response = await useNuxtApp().$grantAlarmsEndpoint.read(id, params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchGrantAlarm response', response)
                this.grantAlarm = response
            }
            else {
                console.log('zone.fetchGrantAlarm error', response)
            }
        },
        async fetchElement(id, params = null) {
            let ctx = this
            let response = await useNuxtApp().$elementsEndpoint.read(id, params, {
                async onResponse({ response }) {
                    ctx.element.editable = response.headers.get('X-Api-Can-Edit') || false
                }})
            if (typeof response !== 'undefined') {
                console.log('zone.fetchElement response', response)
                this.element.element = response
                console.log('zone.fetchElement response', response, this.element.editable)
            }
            else {
                console.log('zone.fetchElement error', response)
            }
        },
        async fetchSpectrum(id, params = null) {
            let ctx = this
            let response = await useNuxtApp().$spectrumEndpoint.read(id, params, {
                async onResponse({ response }) {
                    ctx.spectrum.editable = response.headers.get('X-Api-Can-Edit') || false
                }})
            if (typeof response !== 'undefined') {
                console.log('zone.fetchSpectrum response', response)
                this.spectrum.spectrum = response
                console.log('zone.fetchSpectrum response', response, this.spectrum.editable)
            }
            else {
                console.log('zone.fetchSpectrum error', response)
            }
        },
        async fetchSpectrums(params = null) {
            let response = await useNuxtApp().$spectrumEndpoint.list(params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchSpectrums response', response)
                this.spectrums = response
                console.log('zone.fetchSpectrums response', response)
            }
            else {
                console.log('zone.fetchSpectrums error', response)
            }
        },
        async fetchGrants(params = null) {
            let response = await useNuxtApp().$grantsEndpoint.list(params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchGrants response', response)
                this.grants = response
                console.log('zone.fetchGrants response', response)
            }
            else {
                console.log('zone.fetchGrants error', response)
            }
        },
        async fetchGrant(id, params = null) {
            let ctx = this
            let response = await useNuxtApp().$grantsEndpoint.read(id, params, {
                async onResponse({ response }) {
                    ctx.grant.editable = response.headers.get('X-Api-Can-Edit') || false
                }})
            if (typeof response !== 'undefined') {
                console.log('zone.fetchGrant response', response)
                this.grant.grant = response
                console.log('zone.fetchGrant response', response, this.grant.editable)
            }
            else {
                console.log('zone.fetchGrant error', response)
            }
        },
        async fetchAntenna(id, params = null) {
            let ctx = this
            let response = await useNuxtApp().$antennasEndpoint.read(id, params, {
                async onResponse({ response }) {
                    ctx.antenna.editable = response.headers.get('X-Api-Can-Edit') || false
                }})
            if (typeof response !== 'undefined') {
                console.log('zone.fetchAntenna response', response)
                this.antenna.antenna = response
                console.log('zone.fetchAntenna response', response, this.antenna.editable)
            }
            else {
                console.log('zone.fetchAntenna error', response)
            }
        },
        async fetchMonitors(params = null) {
            let response = await useNuxtApp().$monitorsEndpoint.list(params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchMonitors response', response)
                this.monitors = response
                console.log('zone.fetchMonitors response', response)
            }
            else {
                console.log('zone.fetchMonitors error', response)
            }
        },
        async fetchMonitor(id, params = null) {
            let ctx = this
            let response = await useNuxtApp().$monitorsEndpoint.read(id, params, {
                async onResponse({ response }) {
                    ctx.monitor.editable = response.headers.get('X-Api-Can-Edit') || false
                }})
            if (typeof response !== 'undefined') {
                console.log('zone.fetchMonitor response', response)
                this.monitor.monitor = response
                console.log('zone.fetchMonitor response', response, this.monitor.editable)
            }
            else {
                console.log('zone.fetchMonitor error', response)
            }
        },
        async fetchUsers(params = null) {
            let response = await useNuxtApp().$usersEndpoint.list(params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchUsers response', response)
                this.users = response
                console.log('zone.fetchUsers response', response)
            }
            else {
                console.log('zone.fetchUsers error', response)
            }
        },
        async fetchAntennas(params = null) {
            let response = await useNuxtApp().$antennasEndpoint.list(params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchAntennas response', response)
                this.antennas = response
                console.log('zone.fetchAntennas response', response)
            }
            else {
                console.log('zone.fetchAntennas error', response)
            }
        },
        async fetchLocation(id, params = null) {
            let ctx = this
            let response = await useNuxtApp().$locationsEndpoint.read(id, params, {
                async onResponse({ response }) {
                    ctx.location.editable = response.headers.get('X-Api-Can-Edit') || false
                }})
            if (typeof response !== 'undefined') {
                console.log('zone.fetchLocation response', response)
                this.location.location = response
                console.log('zone.fetchLocation response', response, this.location.editable)
            }
            else {
                console.log('zone.fetchLocation error', response)
            }
        },
        async fetchLocations(params = null) {
            let response = await useNuxtApp().$locationsEndpoint.list(params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchLocations response', response)
                this.locations = response
                console.log('zone.fetchLocations response', response)
            }
            else {
                console.log('zone.fetchLocations error', response)
            }
        },
        async fetchZones(params = null) {
            let response = await useNuxtApp().$zonesEndpoint.list(params)
            if (typeof response !== 'undefined') {
                console.log('zone.fetchZones response', response)
                this.zones = response
                if (this.zones.zones && this.zones.zones.length === 1)
                    this.zone.zone = this.zones.zones[0]
                console.log('zone.fetchZones response', response)
            }
            else {
                console.log('zone.fetchZones error', response)
            }
        },
        computeZoneCenter() {
            if (!this.zone?.zone || !this.zone?.zone?.area?.points)
                return null

            var points = this.zone.zone.area.points
            var min_x  = 180
            var max_x  = -180
            var min_y  = 90
            var max_y  = -90

            points.forEach(function (point) {
                var x = point.x
                var y = point.y

                if (x < min_x) min_x = x;
                if (x > max_x) max_x = x;
                if (y < min_y) min_y = y;
                if (y > max_y) max_y = y;
            });
            var x = min_x + (max_x - min_x)/2
            var y = min_y + (max_y - min_y)/2

            return [y, x]
        },
        computeZoneBoundary() {
            if (!this.zone?.zone || !this.zone?.zone?.area?.points)
                return null

            return this.zone.zone.area.points.map(v => [v.y, v.x])
        },
    },
})
