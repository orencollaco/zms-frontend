import { defineStore } from 'pinia'

export const adminStore = defineStore('admin', {
    state: () => ({
        initialized: false,
        users: {
            users: [],
            page: 1,
            pages: 1,
            total: 0
        },
        elements: {
            elements: [],
            page: 1,
            pages: 1,
            total: 0
        },
        all_elements: {
            elements: [],
            page: 1,
            pages: 1,
            total: 0
        },
        all_elements_by_id: {},
        all_locations: {
            locations: [],
            page: 1,
            pages: 1,
            total: 0
        },
        all_locations_by_id: {},
        all_spectrum: {
            spectrum: [],
            page: 1,
            pages: 1,
            total: 0
        },
        all_spectrum_by_id: {},
        requests: {
            requests: [],
            page: 1,
            pages: 1,
            total: 0
        },
        services: {
            services: [],
            page: 1,
            pages: 1,
            total: 0
        },
        spectrum: {
            spectrum: [],
            page: 1,
            pages: 1,
            total: 0
        },
        grants: {
            grants: [],
            page: 1,
            pages: 1,
            total: 0
        },
        claims: {
            claims: [],
            page: 1,
            pages: 1,
            total: 0
        },
        radios: {
            radios: [],
            page: 1,
            pages: 1,
            total: 0
        },
        antennas: {
            antennas: [],
            page: 1,
            pages: 1,
            total: 0
        },
        monitors: {
            monitors: [],
            page: 1,
            pages: 1,
            total: 0
        },
        observations: {
            observations: [],
            page: 1,
            pages: 1,
            total: 0,
        },
        observation: {
            observation: null
        },
        propsims: {
            propsims: [],
            page: 1,
            pages: 1,
            total: 0,
        },
        grant_alarms: {
            grant_alarms: [],
            page: 1,
            pages: 1,
            total: 0,
        },
    }),
    actions: {
        logout() {
            this.$reset()
        },
        adminOff() {
            this.$reset()
        },
        async initAdmin() {
            if (this.initialized) {
                console.log("skipping initAdmin; already bootstrapped all_elements list", this.all_elements.elements.length)
                return
            }

            console.log("initAdmin: start")
            await this.fetchAllElements()
            await this.fetchAllSpectrum()
            await this.fetchAllLocations()
            console.log("initAdmin: done")
            this.initialized = true
        },
        async fetchUsers(params) {
            let response = await useNuxtApp().$usersEndpoint.list({
                ...params
            })
            if (typeof response !== 'undefined') {
                this.users = response
                console.log('fetched users (admin)', this.users.users.length)
            }
        },
        async fetchElements(params) {
            let response = await useNuxtApp().$elementsEndpoint.list({
                ...params
            })
            if (typeof response !== 'undefined') {
                this.elements = response
                console.log('fetched elements (admin)', this.elements.elements.length)
            }
        },
        async fetchServices(params) {
            let response = await useNuxtApp().$servicesEndpoint.list({
                ...params
            })
            if (typeof response !== 'undefined') {
                this.services = response
                console.log('fetched services (admin)', this.services.services.length)
            }
        },
        async fetchSpectrum(params) {
            let response = await useNuxtApp().$spectrumEndpoint.list({
                ...params
            })
            if (typeof response !== 'undefined') {
                this.spectrum = response
                console.log('fetched spectrum (admin)', this.spectrum.spectrum.length)
            }
        },
        async fetchGrants(params) {
            let response = await useNuxtApp().$grantsEndpoint.list({
                ...params
            })
            if (typeof response !== 'undefined') {
                this.grants = response
                console.log('fetched grants (admin)', this.grants.grants.length)
            }
        },
        async fetchClaims(params) {
            let response = await useNuxtApp().$claimsEndpoint.list({
                ...params
            })
            if (typeof response !== 'undefined') {
                this.claims = response
                console.log('fetched claims (admin)', this.claims.claims.length)
            }
        },
        async fetchRadios(params) {
            let response = await useNuxtApp().$radiosEndpoint.list({
                ...params
            })
            if (typeof response !== 'undefined') {
                this.radios = response
                console.log('fetched radios (admin)', this.radios.radios.length)
            }
        },
        async fetchAntennas(params) {
            let response = await useNuxtApp().$antennasEndpoint.list({
                ...params
            })
            if (typeof response !== 'undefined') {
                this.antennas = response
                console.log('fetched antennas (admin)', this.antennas.antennas.length)
            }
        },
        async fetchMonitors(params) {
            let response = await useNuxtApp().$monitorsEndpoint.list({
                ...params
            })
            if (typeof response !== 'undefined') {
                this.monitors = response
                console.log('fetched monitors (admin)', this.monitors.monitors.length)
            }
        },
        async fetchObservations(params) {
            let response = await useNuxtApp().$observationsEndpoint.list({
                ...params
            })
            if (typeof response !== 'undefined') {
                console.log("f", response);
                let old_items_per_page = this.observations.items_per_page
                this.observations = response
                console.log('fetched observations (admin)', this.observations.observations.length)
                this.observations.items_per_page = params.items_per_page || old_items_per_page
            }
        },
        async fetchObservation(id, params) {
            let response = await useNuxtApp().$observationsEndpoint.read(id, {
                ...params
            })
            if (typeof response !== 'undefined') {
                this.observation = { "observation" : response };
                console.log('fetched observation (admin)', id, this.observation)
            }
        },
        async fetchPropsims(params) {
            let response = await useNuxtApp().$propsimsEndpoint.list({
                ...params
            })
            if (typeof response !== 'undefined') {
                console.log("f", response);
                let old_items_per_page = this.propsims.items_per_page
                this.propsims = response
                console.log('fetched propsims (admin)', this.propsims.propsims.length)
                this.propsims.items_per_page = params.items_per_page || old_items_per_page
            }
        },
        async fetchGrantAlarms(params) {
            let response = await useNuxtApp().$grantAlarmsEndpoint.list({
                ...params
            })
            if (typeof response !== 'undefined') {
                let old_items_per_page = this.grant_alarms.items_per_page
                this.grant_alarms = response
                console.log('fetched grantAlarms (admin)', this.grant_alarms.grant_alarms.length)
                this.grant_alarms.items_per_page = params.items_per_page || old_items_per_page
            }
        },
        async fetchAllElements(params) {
            let response = await useNuxtApp().$elementsEndpoint.list({
                page: 1,
                items_per_page: 65536
            })
            if (typeof response !== 'undefined') {
                this.all_elements = response
                console.log('fetched all_elements (admin)',
                            this.all_elements.elements.length)
                this.all_elements_by_id = {}
                for (var i = 0; i < this.all_elements.elements.length; ++i) {
                    this.all_elements_by_id[this.all_elements.elements[i].id] = this.all_elements.elements[i]
                }
            }
        },
        getElementNameById(id) {
            if (this.all_elements_by_id[id] !== undefined)
                return this.all_elements_by_id[id].name
            return ""
        },
        async fetchAllLocations(params) {
            let response = await useNuxtApp().$locationsEndpoint.list({
                page: 1,
                items_per_page: 65536
            })
            if (typeof response !== 'undefined') {
                this.all_locations = response
                console.log('fetched all_locations (admin)',
                            this.all_locations.locations.length)
                this.all_locations_by_id = {}
                for (var i = 0; i < this.all_locations.locations.length; ++i) {
                    this.all_locations_by_id[this.all_locations.locations[i].id] = this.all_locations.locations[i]
                }
            }
        },
        getLocationNameById(id) {
            if (this.all_locations_by_id[id] !== undefined)
                return this.all_locations_by_id[id].name
            return ""
        },
        async fetchAllSpectrum(params) {
            if (params === undefined)
                params = { page: 1, items_per_page: 65536 }
            let response = await useNuxtApp().$spectrumEndpoint.list(params)
            if (typeof response !== 'undefined') {
                this.all_spectrum = response
                console.log('fetched all_spectrum (admin)',
                            this.all_spectrum.spectrum.length)
                this.all_spectrum_by_id = {}
                for (var i = 0; i < this.all_spectrum.spectrum.length; ++i) {
                    let e = this.all_spectrum.spectrum[i]
                    this.all_spectrum_by_id[e.id] = e
                }
            }
        },
        getSpectrumNameById(id) {
            if (this.all_spectrum_by_id[id] !== undefined)
                return this.all_spectrum_by_id[id].name
            return ""
        }
    }
})
