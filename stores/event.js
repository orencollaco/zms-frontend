
import { defineStore } from 'pinia'
import { ZmsSubscription, ZmsEvent } from '~/lib/event.js'

const services = [ "identity", "zmc", "dst", "alarm" ]

export const eventStore = defineStore('event', {
    state: () => ({
        connection: {
            auto: false,
            reconnect: true,
        },
        events: {
            events: [],
            total: 0,
            limit: 100,
            page: 1,
            itemsPerPage: 20,
            unread: 0,
        },
        alerts: {
            levels: [ 'error', 'warning', 'success' ],
            timeout: 8,
        },
        subscriptionsConfig: {
            identity: {
                enabled: true,
            },
            zmc: {
                enabled: true,
            },
            dst: {
                enabled: true,
            },
            alarm: {
                enabled: true,
            },
            ra: {
                enabled: true,
            },
        },
        subscriptions: {
            identity: {
                socket: null,
                handlers: [],
                retryTimeout: null,
                retryInterval: 8,
            },
            zmc: {
                socket: null,
                handlers: [],
                retryTimeout: null,
                retryInterval: 8,
            },
            dst: {
                socket: null,
                handlers: [],
                retryTimeout: null,
                retryInterval: 8,
            },
            alarm: {
                socket: null,
                handlers: [],
                retryTimeout: null,
                retryInterval: 8,
            },
            ra: {
                socket: null,
                handlers: [],
                retryTimeout: null,
                retryInterval: 8,
            },
        },
    }),
    persist: {
	storage: persistedState.localStorage,
        paths: [ 'connection', 'events', 'alerts', 'subscriptionsConfig' ],
        // Use a custom deserializer so that our stored events are hydrated
        // back into ZmsEvent objects, so that its class helper methods are
        // available immediately.
        serializer: {
            serialize: JSON.stringify,
            deserialize: (s) => {
                try {
                    let j = JSON.parse(s)
                    if (j.events.events) {
                        // Convert stringified events to Event objects on store
                        // hydration on page load.
                        let hydratedEvents = []
                        for (var i in j.events.events) {
                            let newEvt = new ZmsEvent(j.events.events[i])
                            console.log("eventStore.deserialize: event", newEvt)
                            hydratedEvents.push(newEvt)
                        }
                        j.events.events = hydratedEvents
                    }
                    return j
                } catch (e) {
                    console.log("Error hydrating eventStore:", e)
                }
            },
        },
    },
    actions: {
        initSubscriptions() {
            for (const i in services) {
                const name = services[i]
                if (this.subscriptions[name].socket !== null)
                    continue
                let msgHandler = (event) => {
                    this.events.events.unshift(event)
                    while (this.events.events.length > this.events.limit)
                        this.events.events.pop()
                    // Invoke handlers.
                    for (const i in this.subscriptions[name].handlers)
                        this.subscriptions[name].handlers[i](event)
                }
                let errHandler = (event) => {
                    if (event.code == 401 || event.code == 403) {
                        console.log(`eventStore.subscription[${name}].errHandler: auth error (${event.code}); not retrying`, event)
                        if (this.subscriptions[name].retryTimeout) {
                            clearTimeout(this.subscriptions[name].retryTimeout)
                            this.subscriptions[name].retryTimeout = null
                        }
                        this.subscriptions[name].retryInterval = 8
                    }
                    else {
                        if (!this.connection.reconnect) {
                            console.log(`eventStore.subscription[${name}].errHandler: reconnect disabled, not retrying`)
                            this.subscriptions[name].retryTimeout = null
                            this.subscriptions[name].retryInterval = 8
                        }
                        else {
                            let ri = this.subscriptions[name].retryInterval
                            console.log(`eventStore.subscription[${name}].errHandler: retrying (code=${event.code}, retryInterval=${ri}`, event)  

                            this.subscriptions[name].retryTimeout = setTimeout(
                                () => { this.subscriptions[name].socket.open() },
                                ri * 1000)
                            this.subscriptions[name].retryInterval = Math.min(
                                this.subscriptions[name].retryInterval * 2, 60)
                        }
                    }
                }
                this.subscriptions[name].socket = new ZmsSubscription(
                    name, msgHandler, errHandler)
            }
        },
        async subscribe(service = null) {
            this.initSubscriptions()

            var targets = services
            if (service) {
                if (!(service in this.subscriptions))
                    throw Error("unknown service " + service)
                targets = [ service ]
            }

            for (const i in targets) {
                const name = targets[i]
                if (!this.subscriptionsConfig[name].enabled)
                    continue
                if (this.subscriptions[name].socket?.connected)
                    continue
                if (this.subscriptions[name].retryTimeout) {
                    clearTimeout(this.subscriptions[name].retryTimeout)
                    this.subscriptions[name].retryTimeout = null
                    this.subscriptions[name].retryInterval = 8
                }
                try {
                    this.subscriptions[name].socket.open()
                    console.log("eventStore.subscribe: subscribing to " + name)
                } catch (e) {
                    console.log("eventStore.subscribe: failed to subscribe to " + name, e)
                }
            }
        },
        async unsubscribe(service = null) {
            var targets = services
            if (service) {
                if (!(service in this.subscriptions))
                    throw Error("unknown service " + service)
                targets = [ service ]
            }

            for (const i in targets) {
                const name = targets[i]
                if (!this.subscriptions[name].socket)
                    continue
                if (this.subscriptions[name].retryTimeout) {
                    clearTimeout(this.subscriptions[name].retryTimeout)
                    this.subscriptions[name].retryTimeout = null
                    this.subscriptions[name].retryInterval = 8
                }
                try {
                    await this.subscriptions[name].socket.close()
                    console.log("eventStore.unsubscribe: closed subscription websocket for " + name)
                } catch (e) {
                    console.log("eventStore.unsubscribe: error closing subscription websocket for " + name + "; ignoring", e)
                }
            }
        },
        clearEvents() {
            this.events.events.splice(0, this.events.events.length)
        },
        isSubscribed() {
            for (const [name, sub] of Object.entries(this.subscriptions)) {
                if (sub.socket?.connected || sub.socket?.status !== "disconnected") {
                    return true
                }
            }
            return false
        },
        addMsgHandler(msgHandler, service = null, autoSubscribe = true) {
            console.log("eventStore.addMsgHandler:", msgHandler, service)

            if (autoSubscribe)
                this.subscribe(service)

            var targets = services
            if (service) {
                if (!(service in this.subscriptions))
                    throw Error("unknown service " + service)
                targets = [ service ]
            }

            for (const i in targets) {
                const name = targets[i]
                this.subscriptions[name].handlers.push(msgHandler)
            }
        },
        removeMsgHandler(msgHandler, service = null) {
            var targets = services
            if (service) {
                if (!(service in this.subscriptions))
                    throw Error("unknown service " + service)
                targets = [ service ]
            }

            for (const i in targets) {
                const name = targets[i]
                let idx = this.subscriptions[name].handlers.indexOf(msgHandler)
                if (idx < 0)
                    continue
                this.subscriptions[name].handlers.splice(idx, 1)
            }
        }
    }
});
